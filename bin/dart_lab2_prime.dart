import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  print("input your number");
  int? n = int.parse(stdin.readLineSync()!);
  print("This number is prime number ?");
  print(checkPrime(n));
}

bool checkPrime(int n) {
  if (n <= 1) {
    return false;
  }
  for (int i = 3; i < n / 2; i += 2) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
